package io.yorosoft.yourbankingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YourBankingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(YourBankingApiApplication.class, args);
	}

}
